﻿using System;
using System.IO;
using Windows.Data.Json;
using Windows.Storage.Pickers;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Animation;

// Документацию по шаблону элемента "Пустая страница" см. по адресу https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x419

namespace Mankala
{
    /// <summary>
    /// Пустая страница, которую можно использовать саму по себе или для перехода внутри фрейма.
    /// </summary>
    public sealed partial class MainPage
    {
        public MainPage()
        {
            InitializeComponent();

            // ReSharper disable once PossibleNullReferenceException
            cbConfigs.Items.Add(new MankalaConfig { Name = "Browse..." });
            cbConfigs.Items.Add(new MankalaConfig { Name = "Set up..." });
            cbConfigs.Items.Add(new MankalaConfig());
        }

        private void clickStart(object sender, RoutedEventArgs e)
        {
            MankalaConfig mk = cbConfigs.SelectedItem as MankalaConfig;
            if (cbConfigs.SelectedIndex == 1)
                mk = new MankalaConfig {
                    SocketsPerRow = int.Parse(tbSPR.Text),
                    RowsPerPlayer = int.Parse(tbRPP.Text),
                    PlantOffset = int.Parse(tbPO.Text),
                    SocketSeedCount = int.Parse(tbSPS.Text),
                    AddSeedToStorePerRow = cbATS.IsChecked ?? false,
                    CaptureIfFinishOnYourSide = cbCO.IsChecked ?? false,
                    RepeatTurnIfLastSeedIsInStore = cbRIF.IsChecked ?? false
                };

            Frame.Navigate(typeof(GamePage), mk, new SlideNavigationTransitionInfo());
        }

        private async void clickSelect(object sender, SelectionChangedEventArgs e)
        {
            grOpts.Visibility = Visibility.Collapsed;
            if (cbConfigs.SelectedIndex == 0) {
                var file = await new FileOpenPicker { FileTypeFilter = { ".mankala" }, ViewMode = PickerViewMode.List }
                    .PickSingleFileAsync();
                if (file is null) return;
                var str = await file.OpenStreamForReadAsync();
                if (str is null) return;
                using (var sr = new StreamReader(str)) {
                    // ReSharper disable once PossibleNullReferenceException
                    cbConfigs.Items.Add(MankalaConfig.FromJObject(JsonObject.Parse(sr.ReadToEnd())));
                    cbConfigs.SelectedIndex = cbConfigs.Items.Count;
                }
            } else if (cbConfigs.SelectedIndex == 1) {
                grOpts.Visibility = Visibility.Visible;
            }
        }

        private async void clickSave(object sender, RoutedEventArgs e)
        {
            var mk = new MankalaConfig {
                SocketsPerRow = int.Parse(tbSPR.Text),
                RowsPerPlayer = int.Parse(tbRPP.Text),
                PlantOffset = int.Parse(tbPO.Text),
                SocketSeedCount = int.Parse(tbSPS.Text),
                AddSeedToStorePerRow = cbATS.IsChecked ?? false,
                CaptureIfFinishOnYourSide = cbCO.IsChecked ?? false,
                RepeatTurnIfLastSeedIsInStore = cbRIF.IsChecked ?? false
            };
            try {
                var fl = await new FileSavePicker { DefaultFileExtension = ".mankala", SuggestedStartLocation = PickerLocationId.Downloads }.PickSaveFileAsync();
                mk.Name = fl.DisplayName;
                using (var sw = new StreamWriter(await fl.OpenStreamForWriteAsync()))
                    sw.Write(mk.Serialize().Stringify());
                // ReSharper disable once PossibleNullReferenceException
                cbConfigs.Items.Add(mk);
                cbConfigs.SelectedIndex = cbConfigs.Items.Count;
            }
            catch
            {
                // ignored
            }
        }
    }
}
