﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Windows.UI.Core;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;
using Mankala.Annotations;

#pragma warning disable CS4014 // Так как этот вызов не ожидается, выполнение существующего метода продолжается до завершения вызова
// Документацию по шаблону элемента "Пустая страница" см. по адресу https://go.microsoft.com/fwlink/?LinkId=234238

namespace Mankala
{
    /// <summary>
    /// Пустая страница, которую можно использовать саму по себе или для перехода внутри фрейма.
    /// </summary>
    public sealed partial class GamePage : INotifyPropertyChanged
    {
        private MankalaConfig config;
        private int[] sockets;
        private int _score0;
        private int _score1;
        private int currentPlayer;
        private bool _lock;
        private readonly List<int>[] indices = { new List<int>(), new List<int>() };

        private int Score0
        {
            get => _score0;
            set {
                if (value == _score0) return;
                _score0 = value;
                OnPropertyChanged();
            }
        }

        private int Score1
        {
            get => _score1;
            set {
                if (value == _score1) return;
                _score1 = value;
                OnPropertyChanged();
            }
        }

        public GamePage()
        {
            InitializeComponent();
        }

        private void SocketClick(object sender, RoutedEventArgs args)
        {
            if (_lock) return;
            _lock = true;
            // ReSharper disable once PossibleNullReferenceException
            var pos = (int)(sender as Button).Tag;
            if (pos / config.SocketsPerPlayer != currentPlayer) {
                new MessageDialog("select appropriate socket, please", "wrong socket").ShowAsync();
                _lock = false;
                return;
            }
            var instore = false;
            var seeds = sockets[pos];
            sockets[pos] = 0;
            if (indices[currentPlayer].Contains(pos)) {
                seeds--;
                Inc();
                if (seeds == 0) {
                    instore = true;
                    if (config.RepeatTurnIfLastSeedIsInStore) currentPlayer -= 1;
                }
            }
            pos = (pos + config.PlantOffset) % sockets.Length;
            Task.Run(() => {
                while (seeds > 0) {
                    if (seeds > 0) {
                        sockets[pos]++;
                        seeds--;
                    }

                    if (indices[currentPlayer].Contains(pos)) {
                        seeds--;
                        Inc();
                        if (seeds == 0) {
                            instore = true;
                            if (config.RepeatTurnIfLastSeedIsInStore) currentPlayer -= 1;
                        }
                    }

                    Dispatcher.RunAsync(CoreDispatcherPriority.Normal, Refresh);
                    pos = (pos + 1) % sockets.Length;
                    Task.Delay(160).Wait();
                }

                pos = (sockets.Length + pos - 1) % sockets.Length;
                if (pos / (config.SocketsPerRow * config.RowsPerPlayer) == currentPlayer &&
                    config.CaptureIfFinishOnYourSide &&
                    !instore &&
                    sockets[pos] == 1) {
                    var id = sockets.Length - 1 - pos;
                    Inc(sockets[id]);
                    sockets[id] = 0;
                }
                if (sockets.Take(sockets.Length / 2).All(x => x == 0)
                 || sockets.Skip(sockets.Length / 2).All(x => x == 0)) {
                    Dispatcher.RunAsync(CoreDispatcherPriority.Normal, Refresh);
                    Dispatcher.RunAsync(CoreDispatcherPriority.Normal, View);
                    var dialog = new MessageDialog("", "game ended!");
                    dialog.Commands.Add(new UICommand("ok", _ => Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => Frame.GoBack(new SlideNavigationTransitionInfo()))));
                    Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => dialog.ShowAsync());
                    return;
                }
                currentPlayer = (currentPlayer + 1) % 2;
                Dispatcher.RunAsync(CoreDispatcherPriority.Normal, Refresh);
                Dispatcher.RunAsync(CoreDispatcherPriority.Normal, View);
                _lock = false;
            });
        }

        private void View()
        {
            bs0.BorderThickness = new Thickness(currentPlayer == 0 ? 2 : 0);
            bs1.BorderThickness = new Thickness(currentPlayer == 0 ? 0 : 2);
        }

        private void Inc(int value = 1)
        {
            if (currentPlayer == 0) Score0 += value;
            else Score1 += value;
        }

        private void Refresh()
        {
            foreach (var panel in spSockets.Children.OfType<StackPanel>())
                foreach (var button in panel.Children.OfType<Button>())
                    button.Content = sockets[(int)button.Tag];
            tbs1.Text = Score1.ToString();
            tbs0.Text = Score0.ToString();
        }

        #region Overrides of Page

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            Configure(e);
            Refresh();
            View();
        }

        private void Configure(NavigationEventArgs e)
        {
            config = e.Parameter as MankalaConfig ?? new MankalaConfig();
            sockets = new int[config.SocketsPerPlayer * 2];

            for (var i = 0; i < sockets.Length; i++)
                sockets[i] = config.SocketSeedCount;
            bool dir = true;
            for (int r = 0; r < config.RowsPerPlayer * 2; r++) {
                var sp = new StackPanel { HorizontalAlignment = HorizontalAlignment.Center, Orientation = Orientation.Horizontal };
                for (int s = 0; s < config.SocketsPerRow; s++) {
                    var i = s + r * config.SocketsPerRow;
                    var b = new Button { Tag = i, Content = config.SocketSeedCount };
                    b.Click += SocketClick;
                    if (dir) sp.Children.Add(b);
                    else sp.Children.Insert(0, b);
                }
                dir ^= true;
                spSockets.Children.Add(sp);
            }

            var or = (config.AddSeedToStorePerRow ? config.SocketsPerRow : config.SocketsPerPlayer) - 1;
            var st = config.AddSeedToStorePerRow ? config.SocketsPerRow : config.SocketsPerPlayer;
            while (or < config.SocketsPerPlayer) {
                indices[0].Add(or);
                or += st;
            }

            indices[1].AddRange(indices[0].Select(x => x + config.SocketsPerPlayer));
        }

        #endregion

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
#pragma warning restore CS4014 // Так как этот вызов не ожидается, выполнение существующего метода продолжается до завершения вызова
