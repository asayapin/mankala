﻿using Windows.Data.Json;

namespace Mankala
{
    public class MankalaConfig
    {
        public int SocketSeedCount { get; set; } = 6;
        public int RowsPerPlayer { get; set; } = 1;
        public int SocketsPerRow { get; set; } = 6;
        public int PlantOffset { get; set; } = 1;
        public bool RepeatTurnIfLastSeedIsInStore { get; set; } = true;
        public bool CaptureIfFinishOnYourSide { get; set; } = true;
        /// <summary>
        /// per row or per player seed addition behavior
        /// </summary>
        public bool AddSeedToStorePerRow { get; set; } 
        public string Name { private get; set; } = "Default";

        public int SocketsPerPlayer => RowsPerPlayer * SocketsPerRow;

        #region Overrides of Object

        public override string ToString() => Name;

        #endregion

        public static MankalaConfig FromJObject(JsonObject jo)
        {
            return new MankalaConfig
            {
                SocketsPerRow = (int)jo.GetObject().GetNamedNumber(nameof(SocketsPerRow)),
                RowsPerPlayer = (int)jo.GetObject().GetNamedNumber(nameof(RowsPerPlayer)),
                SocketSeedCount = (int)jo.GetObject().GetNamedNumber(nameof(SocketSeedCount)),
                Name = jo.GetObject().GetNamedString(nameof(Name)),
                CaptureIfFinishOnYourSide = jo.GetObject().GetNamedBoolean(nameof(CaptureIfFinishOnYourSide)),
                AddSeedToStorePerRow = jo.GetObject().GetNamedBoolean(nameof(AddSeedToStorePerRow)),
                RepeatTurnIfLastSeedIsInStore = jo.GetObject().GetNamedBoolean(nameof(RepeatTurnIfLastSeedIsInStore)),
                PlantOffset = (int)jo.GetObject().GetNamedNumber(nameof(PlantOffset))
            };
        }

        public JsonObject Serialize()
        {
            return new JsonObject {
                [nameof(SocketSeedCount)] = JsonValue.CreateNumberValue(SocketSeedCount),
                [nameof(RowsPerPlayer)] = JsonValue.CreateNumberValue(RowsPerPlayer),
                [nameof(SocketsPerRow)] = JsonValue.CreateNumberValue(SocketsPerRow),
                [nameof(PlantOffset)] = JsonValue.CreateNumberValue(PlantOffset),
                [nameof(RepeatTurnIfLastSeedIsInStore)] = JsonValue.CreateBooleanValue(RepeatTurnIfLastSeedIsInStore),
                [nameof(CaptureIfFinishOnYourSide)] = JsonValue.CreateBooleanValue(CaptureIfFinishOnYourSide),
                [nameof(AddSeedToStorePerRow)] = JsonValue.CreateBooleanValue(AddSeedToStorePerRow),
                [nameof(Name)] = JsonValue.CreateStringValue(Name),
            };
        }
    }
}
